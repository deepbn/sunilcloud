var express = require('express');
var router = express.Router();
var ctrlHome = require('../controllers/home');
var ctrlUser = require('../controllers/users');
/* GET home page. */

module.exports = function (passport) {
    router.get('/', ctrlHome.getHomePage);

    router.get('/login', ctrlUser.getLoginPage);

    router.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        //failureFlash : true // allow flash messages
    }));

    router.get('/signup', ctrlUser.getSignupPage);

    router.post('/signup',  passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        //failureFlash : true // allow flash messages
    }));

    router.get('/profile', isLoggedIn, ctrlUser.getProfilePage);

    router.get('/logout', ctrlUser.logout);

    return router;
    
};


function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
