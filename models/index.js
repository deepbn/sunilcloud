/**
 * This file is to require all files except the index.js once.
 * So that only requiring the Directory will automatically include all Modules in the Directory
 */
require('fs').readdirSync(__dirname).forEach(function (file) {
    /* If its the index file ignore it */
    if (file !== 'index.js'){
        require('./'+file);
    }

});