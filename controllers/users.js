
module.exports.getUsers = function (req,res) {
    res.send('respond with a resource');
};

module.exports.getLoginPage = function (req,res) {
    res.render('login', { title: 'Sunil Cloud' });
};

module.exports.getSignupPage = function (req,res) {
    res.render('signup', { title: 'Sunil Cloud' });
};

module.exports.getProfilePage = function (req,res) {
    res.render('profile', { user: req.user });
};

module.exports.logout = function (req,res) {
    req.logout();
    res.redirect('/');
};